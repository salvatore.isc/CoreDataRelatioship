//
//  ViewController.swift
//  CoreDataRelatioship
//
//  Created by Salvador Lopez on 16/06/23.
//

import UIKit
import CoreData

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        cargaPersona()
        //obtenerPersonasConRutas()
    }
    
    func cargaPersona() {
        // Instancia AppDelegate
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        // Obtener Contexto
        let manageContext = appDelegate.persistentContainer.viewContext
        
        // Nueva Entity Persona
        let personaEntity = NSEntityDescription.entity(forEntityName: "Persona", in: manageContext)
        
        // Crear registro con la entidad Persona
        let persona = NSManagedObject(entity: personaEntity!, insertInto: manageContext)
        persona.setValue("02/02/89", forKey: "fechaNacimiento")
        persona.setValue("example@example.com", forKey: "correo")
        persona.setValue("123", forKey: "numeroEmpleado")
        persona.setValue("John Doe", forKey: "nombre")
        persona.setValue("password", forKey: "contrasenia")
        persona.setValue("1234567890", forKey: "telefono")
        persona.setValue("Admin", forKey: "puestoEmpleado")
        
        // Nueva Entity Ruta
        let rutaEntity = NSEntityDescription.entity(forEntityName: "Ruta", in: manageContext)
        
        // Crear registro con la entidad Ruta
        let ruta = NSManagedObject(entity: rutaEntity!, insertInto: manageContext)
        ruta.setValue("123 Main St", forKey: "direccion")
        ruta.setValue("Persona 1", forKey: "personaVisita")
        ruta.setValue("Nota", forKey: "nota")
        ruta.setValue("$1000", forKey: "adeudo")
        
        // Establecer la relación entre persona y ruta
        let rutasSet = persona.mutableSetValue(forKey: "ruta")
        rutasSet.add(ruta)
        
        
        // Guardar
        do {
            try manageContext.save()
            print("Guardado con éxito...")
        } catch {
            print("Error: \(error)")
        }
    }


    func obtenerPersonasConRutas() {
        // Instancia AppDelegate
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        // Obtener Contexto
        let manageContext = appDelegate.persistentContainer.viewContext
        
        // Crear la solicitud de búsqueda de Personas
        let fetchRequestPersona = NSFetchRequest<NSFetchRequestResult>(entityName: "Persona")
        
        do {
            // Obtener el array de resultados de Personas
            let personas = try manageContext.fetch(fetchRequestPersona) as! [NSManagedObject]
            
            // Iterar sobre las Personas
            for persona in personas {
                // Obtener los atributos de la Persona
                let fechaNacimiento = persona.value(forKey: "fechaNacimiento") as! String
                let correo = persona.value(forKey: "correo") as! String
                let numeroEmpleado = persona.value(forKey: "numeroEmpleado") as! String
                let nombre = persona.value(forKey: "nombre") as! String
                let contrasenia = persona.value(forKey: "contrasenia") as! String
                let telefono = persona.value(forKey: "telefono") as! String
                let puestoEmpleado = persona.value(forKey: "puestoEmpleado") as! String
                
                print("Persona:")
                print("Fecha de nacimiento: \(fechaNacimiento)")
                print("Correo: \(correo)")
                print("Número de empleado: \(numeroEmpleado)")
                print("Nombre: \(nombre)")
                print("Contraseña: \(contrasenia)")
                print("Teléfono: \(telefono)")
                print("Puesto de empleado: \(puestoEmpleado)")
                
                // Obtener las rutas relacionadas
                if let rutas = persona.value(forKey: "rutas") as? Set<NSManagedObject> {
                    // Iterar sobre las rutas relacionadas
                    for ruta in rutas {
                        // Obtener los atributos de la Ruta
                        let direccion = ruta.value(forKey: "direccion") as! String
                        let personaVisita = ruta.value(forKey: "personaVisita") as! String
                        let nota = ruta.value(forKey: "nota") as! String
                        let adeudo = ruta.value(forKey: "adeudo") as! String
                        
                        print("Ruta:")
                        print("Dirección: \(direccion)")
                        print("Persona que visita: \(personaVisita)")
                        print("Nota: \(nota)")
                        print("Adeudo: \(adeudo)")
                    }
                }
                
                print("---")
            }
            
        } catch {
            print("Error: \(error)")
        }
    }




}

